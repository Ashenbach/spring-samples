package org.cuatrovientos.jdbc.sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.sql.PreparedStatement;

/**
 * Simple class to access relational db
 *
 */
public class App  {
    public static void main( String[] args ) {
        try {
        	Class.forName("com.mysql.jdbc.Driver");
        	Connection connection = 
        			DriverManager.getConnection("jdbc:mysql://localhost/todo","root","");
        	Statement statement = 
        			connection.createStatement();
        	
        	ResultSet resultset =
        			statement.executeQuery("select * from task");
        	
        	while (resultset.next()) {
        		System.out.println("Id: " + resultset.getInt("id"));
        		System.out.println("Task: " + resultset.getString("task"));
        	}
        	
        	int queryResult = 0;
        	
        	//insert
        	queryResult = statement.executeUpdate("insert into task (task) values('poo')");
        	System.out.println(queryResult);
        	
        	//delete
        	queryResult = statement.executeUpdate("delete from task where id > 5");
        	System.out.println(queryResult);
        	
        	//update
        	String task = "PENE";
        	queryResult = statement.executeUpdate("update task set task =' " + task + "'where id > 3");
        	System.out.println(queryResult);
        	
        	PreparedStatement preparedStatement = connection.prepareStatement("insert into task (task) values (?)");
        	
        	preparedStatement.setString(1, "ROLF");
        	preparedStatement.addBatch();
        	preparedStatement.execute();
        	
        	
        	
        	
        	resultset.close();
         	connection.close();
        	
        } catch (Exception e) {
        	System.err.println("Exception : " + e.getMessage());
        }
       
    	
    }
}

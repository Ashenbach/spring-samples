package org.cuatrovientos.javaspringkart;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Inits JavaKart race
 * @author Pello Altadill
 * @greetz for the blue mug
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("javaspringkart.xml");
        
        Race myRace = (Race) context.getBean("myRace");
        
        System.out.println(myRace.toString());
        myRace.run();
        System.out.println(myRace.showResult());
	}

}
